import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffManageComponent } from './staff-manage.component';
import { RouterTestingModule } from '@angular/router/testing';
import { StaffManageModule } from './staff-manage.module';
import { of } from 'rxjs';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Booking, Booking_Room, RoomType, Staff, Staff_Booking } from '../../../shared/interfaces';

const mockStaff: Staff = {
  id: 1,
  firstName: 'firstName',
  lastName: 'lastName',
  rating: 1,
  salary: 1,
  positionId: 1,
};

const mockRoom: RoomType = {
  id: 1,
  type: 'type1',
  price: 3000,
  roomsAmount: 2,
  guestsAmount: 1,
};

const mockBooking: Booking = {
  id: 1,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
};

const mockBookingRoom: Booking_Room = {
  id: 1,
  bookingId: 1,
  roomId: 1,
};

const mockBookingStaff: Staff_Booking = {
  id: 1,
  staffId: 1,
  bookingId: 1,
};

describe('StaffManageComponent', () => {
  let component: StaffManageComponent;
  let fixture: ComponentFixture<StaffManageComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StaffManageModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffManageComponent);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('method "initialize"', () => {
    it('should invoke get http request for staff by its id', () => {
      // Arrange
      const id = '1';

      const httpSpy = spyOn(httpClient, 'get')
        .withArgs(`/api/staff/${id}`)
        .and.returnValue(of([mockStaff]))
        .withArgs('/api/bookings')
        .and.returnValue(of([]))
        .withArgs('/api/booking-staff')
        .and.returnValue(of([]))
        .withArgs('/api/booking-room')
        .and.returnValue(of([]))
        .withArgs('/api/staff')
        .and.returnValue(of([]))
        .withArgs('/api/tours')
        .and.returnValue(of([]));

      // Act
      component.initialize(id);

      // Assert
      expect(httpSpy).toHaveBeenCalledWith(`/api/staff/${id}`);
      expect(component.person).toEqual(mockStaff);
      expect(component.staff).toEqual([]);
      expect(component.tours).toEqual([]);
    });
  });

  describe('method "formatBookings"', () => {
    it('should find rooms of the booking and staff in charge if exists', () => {
      // Arrange
      const bookings: Booking[] = [mockBooking];
      const booking_rooms = [mockBookingRoom];
      const booking_stuff = [mockBookingStaff];
      const staffs = [mockStaff];

      // Act
      const result = component.formatBookings([bookings, booking_stuff, staffs, booking_rooms]);

      // Assert
      expect(result).toEqual([
        {
          id: 1,
          arrivalDate: mockBooking.arrivalDate,
          departureDate: mockBooking.departureDate,
          bill: 1,
          inCharge: mockStaff,
          roomsNums: [mockRoom.id],
        },
      ]);
    });
  });
});
