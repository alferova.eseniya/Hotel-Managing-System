import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakfastsTableComponent } from './breakfasts-table.component';
import { BookingFormModule } from '../../../guests/booking-form/booking-form.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BreakfastsTableModule } from './breakfasts-table.module';

describe('BreakfastsTableComponent', () => {
  let component: BreakfastsTableComponent;
  let fixture: ComponentFixture<BreakfastsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BreakfastsTableComponent],
      imports: [BreakfastsTableModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakfastsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
