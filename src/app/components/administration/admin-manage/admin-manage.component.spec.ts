import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManageComponent } from './admin-manage.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AdminManageModule } from './admin-manage.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Breakfast, Position, Staff, Tour } from '../../../shared/interfaces';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { ConfirmationService } from 'primeng/api';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

const mockStaff: Staff = {
  id: 0,
  firstName: 'firstName',
  lastName: 'lastName',
  rating: 1,
  salary: 1,
  positionId: 1,
};

const mockPosition: Position = {
  id: 0,
  name: 'mockPostiion',
  basicSalary: 100,
};

const mockBreakfast: Breakfast = {
  id: 1,
  menu: 'menu',
  price: 100,
};

const mockTour: Tour = {
  id: 1,
  title: 'tour',
  commonAmount: 2,
  amountOfGuests: 0,
  description: 'description',
  price: 123,
};

describe('AdminManageComponent', () => {
  let component: AdminManageComponent;
  let fixture: ComponentFixture<AdminManageComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let confirmationService: ConfirmationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, NoopAnimationsModule, HttpClientTestingModule, AdminManageModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminManageComponent);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    confirmationService = TestBed.inject(ConfirmationService);

    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('method initialize', () => {
    it('should invoke http requests for breakfasts, staff, positions, tours and sets its value to component', () => {
      //Arrange
      spyOn(httpClient, 'get')
        .withArgs('/api/positions')
        .and.returnValue(of([]))
        .withArgs('/api/staff')
        .and.returnValue(of([mockStaff]))
        .withArgs('/api/breakfasts')
        .and.returnValue(of([]))
        .withArgs('/api/tours')
        .and.returnValue(of([]));

      // Act
      component.initialize();

      // Assert
      expect(httpMock.expectOne({ method: 'get', url: '/api/positions' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/staff' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/breakfasts' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/tours' })).toBeTruthy();

      expect(component.staff).toEqual([mockStaff]);
      expect(component.positions).toEqual([]);
      expect(component.breakfasts).toEqual([]);
      expect(component.tours).toEqual([]);
    });
  });

  describe('method updateSalary', () => {
    it('should invoke http request for updating selected staff salary', () => {
      // Act
      component.updateSalary(mockStaff);

      // Assert
      const { request } = httpMock.expectOne({ method: 'patch', url: '/api/staff' });
      expect(request.method).toBe('PATCH');
      expect(request.body).toEqual(mockStaff);
    });
  });

  describe('method updatePosition', () => {
    it('should invoke http request for updating selected staff position', () => {
      // Act
      component.updatePosition(mockStaff);

      // Assert
      const { request } = httpMock.expectOne({ method: 'patch', url: '/api/staff' });

      expect(request.method).toBe('PATCH');
      expect(request.body).toEqual(mockStaff);
    });
  });

  describe('method addNewStaff', () => {
    // Arrange
    const [firstName, lastName, position] = ['firstName', 'lastName', mockPosition];

    it('shouldn`t invoke http request for creating new staff member if createForm is invalid', () => {
      // Arrange
      component.createForm.patchValue({
        firstName,
        lastName,
      });

      // Act
      component.addNewStaff();

      // Assert
      httpMock.expectNone({ method: 'post', url: '/api/staff' });
      expect(true).toBeTruthy();
    });

    it('should invoke http request  for creating new staff member if createForm is valid', () => {
      // Arrange
      component.createForm.patchValue({
        firstName,
        lastName,
        position,
      });

      // Act
      component.addNewStaff();

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/staff' });

      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ firstName, lastName, positionId: position.id, salary: position.basicSalary });
    });

    it('should add created staff to the component staff array, hide and clear creating form', () => {
      // Arrange
      component.createForm.patchValue({
        firstName,
        lastName,
        position,
      });

      spyOn(httpClient, 'post').and.returnValue(of(mockStaff));

      // Act
      component.addNewStaff();

      // Assert
      expect(component.staff.includes(mockStaff)).toBe(true);
      expect(component.showCreateForm).toBe(false);
      expect(component.createForm.value).toEqual({
        firstName: null,
        lastName: null,
        position: null,
      });
    });
  });

  it('method showCreate should set showCreateForm property to true', () => {
    // Act
    component.showCreate();

    // Assert
    expect(component.showCreateForm).toBe(true);
  });

  it('method submitDescription should update description calling patch http request with "/api/administration" endpoint', () => {
    // Arrange
    const description = 'description';

    component.descriptionForm.patchValue({
      description,
    });

    // Act
    component.submitDescription();

    // Assert
    const { request } = httpMock.expectOne({ method: 'patch', url: '/api/administration' });

    expect(request.method).toBe('PATCH');
    expect(request.body).toEqual({ description });
  });

  describe('method deleteStaff', () => {
    it('should invoke method "config" of confirmationService', () => {
      // Arrange
      const confirmationServiceSpy = spyOn(confirmationService, 'confirm');

      // Act
      component.deleteStaff(mockStaff);

      // Assert
      expect(confirmationServiceSpy).toHaveBeenCalled();
    });

    it('should invoke staff deleting calling delete http request on accept and remove from component staff array', () => {
      // Arrange
      component.staff = [mockStaff];

      // Act
      component.deleteStaff(mockStaff);

      fixture.detectChanges();

      let button = fixture.nativeElement.querySelector('button.p-confirm-dialog-accept')!;

      button.click();

      // Assert
      const { request } = httpMock.expectOne({ method: 'delete', url: '/api/staff' });

      expect(request.method).toBe('DELETE');
      expect(request.body).toEqual({ id: mockStaff.id });
      expect(component.staff.includes(mockStaff)).not.toBe(false);
    });
  });

  describe('method updateMenu', () => {
    it('should invoke http request for updating selected breakfast menu if form valid', () => {
      // Arrange
      component.breakfastForm.patchValue(mockBreakfast);

      // Act
      component.updateMenu(component.breakfastForm.value);

      // Assert
      const { request } = httpMock.expectOne({ method: 'patch', url: '/api/breakfast' });

      expect(request.method).toBe('PATCH');
      expect(request.body).toEqual(mockBreakfast);
    });

    it('shouldn`t invoke http request for updating selected breakfast menu if form invalid', () => {
      // Arrange
      component.breakfastForm.patchValue({ id: 1, menu: '', price: '' });

      // Act
      component.updateMenu(component.breakfastForm.value);

      // Assert
      httpMock.expectNone({ method: 'patch', url: '/api/breakfast' });
      expect(true).toBeTruthy();
    });
  });

  describe('method updatePrice', () => {
    it('should invoke http request for updating selected breakfast price if form valid', () => {
      // Arrange
      component.breakfastForm.patchValue(mockBreakfast);

      // Act
      component.updatePrice(mockBreakfast);

      // Assert
      const { request } = httpMock.expectOne({ method: 'patch', url: '/api/breakfast' });

      expect(request.method).toBe('PATCH');
      expect(request.body).toEqual(mockBreakfast);
    });

    it('shouldn`t invoke http request for updating selected breakfast price if form invalid', () => {
      // Arrange
      component.breakfastForm.patchValue({ menu: '', price: '' });

      // Act
      component.updateMenu(component.breakfastForm.value);

      // Assert
      httpMock.expectNone({ method: 'patch', url: '/api/breakfast' });
      expect(true).toBeTruthy();
    });
  });

  describe('method deleteBreakfast', () => {
    it('should invoke method "config" of confirmationService', () => {
      // Arrange
      const confirmationServiceSpy = spyOn(confirmationService, 'confirm');

      // Act
      component.deleteBreakfast(mockBreakfast);

      // Assert
      expect(confirmationServiceSpy).toHaveBeenCalled();
    });

    it('should invoke breakfast deleting calling delete http request on accept and remove from component breakfasts array', () => {
      // Arrange
      component.breakfasts = [mockBreakfast];

      // Act
      component.deleteBreakfast(mockBreakfast);

      fixture.detectChanges();

      let button = fixture.nativeElement.querySelector('button.p-confirm-dialog-accept')!;

      button.click();

      // Assert
      const { request } = httpMock.expectOne({ method: 'delete', url: '/api/breakfast' });

      expect(request.method).toBe('DELETE');
      expect(request.body).toEqual({ id: mockBreakfast.id });
      expect(component.breakfasts.includes(mockBreakfast)).not.toBe(false);
    });
  });

  describe('method addNewBreakfast', () => {
    // Arrange
    const [menu, price] = ['menu', 123];

    it('shouldn`t invoke http request for creating new breakfast if createBreakfastForm is invalid', () => {
      // Arrange
      component.createBreakfastForm.patchValue({
        menu: '',
        price: '',
      });

      // Act
      component.addNewBreakfast();

      // Assert
      httpMock.expectNone({ method: 'post', url: '/api/breakfasts' });
      expect(true).toBeTruthy();
    });

    it('should invoke http request for creating new breakfast if createBreakfastForm is valid', () => {
      // Arrange
      component.createBreakfastForm.patchValue({
        menu,
        price,
      });

      // Act
      component.addNewBreakfast();

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/breakfasts' });

      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ menu, price });
    });

    it('should add created breakfast to the component breakfasts array, hide and clear creating form', () => {
      // Arrange
      component.createBreakfastForm.patchValue({
        menu,
        price,
      });

      spyOn(httpClient, 'post').and.returnValue(of(mockBreakfast));

      // Act
      component.addNewBreakfast();

      // Assert
      expect(component.breakfasts.includes(mockBreakfast)).toBe(true);
      expect(component.showCreateBreakfastForm).toBe(false);
      expect(component.createBreakfastForm.value).toEqual({
        menu: null,
        price: null,
      });
    });
  });

  describe('method updateTour', () => {
    it('should invoke http request for updating selected tour if form valid', () => {
      // Arrange
      component.tourForm.patchValue(mockTour);

      // Act
      component.updateTour(component.tourForm.value);

      // Assert
      const { request } = httpMock.expectOne({ method: 'patch', url: '/api/tour' });

      expect(request.method).toBe('PATCH');
      expect(request.body).toEqual(mockTour);
    });

    it('shouldn`t invoke http request for updating selected tour if form invalid', () => {
      // Arrange
      component.tourForm.patchValue({ id: 1, title: '', price: '' });

      // Act
      component.updateTour(component.tourForm.value);

      // Assert
      httpMock.expectNone({ method: 'patch', url: '/api/tour' });
      expect(true).toBeTruthy();
    });
  });

  describe('method addNewTour', () => {
    // Arrange
    const [title, commonAmount, amountOfGuests, description, price] = ['tour', 12, 0, 'description', 123];

    it('shouldn`t invoke http request for creating new tour if createTourForm is invalid', () => {
      // Arrange
      component.createTourForm.patchValue({
        title: '',
        commonAmount: '',
        description: '',
        price: '',
      });

      // Act
      component.addNewTour();

      // Assert
      httpMock.expectNone({ method: 'post', url: '/api/tours' });
      expect(true).toBeTruthy();
    });

    it('should invoke http request for creating new tour if createTourForm is valid', () => {
      // Arrange
      component.createTourForm.patchValue({
        title,
        commonAmount,
        description,
        price,
      });

      // Act
      component.addNewTour();

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/tours' });

      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ title, commonAmount, amountOfGuests, description, price });
    });

    it('should add created tour to the component tours array, hide and clear creating form', () => {
      // Arrange
      component.createTourForm.patchValue({
        title,
        commonAmount,
        description,
        price,
      });

      spyOn(httpClient, 'post').and.returnValue(of(mockTour));

      // Act
      component.addNewTour();

      // Assert
      expect(component.tours.includes(mockTour)).toBe(true);
      expect(component.showCreateTourForm).toBe(false);
      expect(component.createTourForm.value).toEqual({
        title: null,
        commonAmount: null,
        description: null,
        price: null,
      });
    });
  });

  describe('method deleteTour', () => {
    it('should invoke method "config" of confirmationService', () => {
      // Arrange
      const confirmationServiceSpy = spyOn(confirmationService, 'confirm');

      // Act
      component.deleteTour(mockTour);

      // Assert
      expect(confirmationServiceSpy).toHaveBeenCalled();
    });

    it('should invoke tour deleting calling delete http request on accept and remove from component tours array', () => {
      // Arrange
      component.tours = [mockTour];

      // Act
      component.deleteTour(mockTour);

      fixture.detectChanges();

      let button = fixture.nativeElement.querySelector('button.p-confirm-dialog-accept')!;

      button.click();

      // Assert
      const { request } = httpMock.expectOne({ method: 'delete', url: '/api/tour' });

      expect(request.method).toBe('DELETE');
      expect(request.body).toEqual({ id: mockTour.id });
      expect(component.tours.includes(mockTour)).not.toBe(false);
    });
  });
});
